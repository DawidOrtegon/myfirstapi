package dfob.data.api.dataAccess;
import dfob.data.api.dataAccess.entities.VideoCassette;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VideoCassetteRepo extends CrudRepository<VideoCassette,Long>
{

}
