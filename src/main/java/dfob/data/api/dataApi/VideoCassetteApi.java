package dfob.data.api.dataApi;

import dfob.data.api.dataAccess.entities.VideoCassette;
import dfob.data.api.manager.VideoCassetteManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController

// Address to connect to the API.
@RequestMapping("/api/cassettes")
public class VideoCassetteApi
{
//    private List<VideoCassette> videoCassettes;
//
//    // Type of constructor filling the list with all the information.
//    public VideoCassetteApi()
//    {
//        videoCassettes = new ArrayList<>();
//        videoCassettes.add(new VideoCassette(1L,"Titanic", LocalDate.of(1995,12,12)));
//        videoCassettes.add(new VideoCassette(2L,"Fine Fiction", LocalDate.of(2000,1,10)));
//        videoCassettes.add(new VideoCassette(3L,"David The Best", LocalDate.of(2001,2,9)));
//    }


    //En vez de la lista pongo una instancia de la clase que manejara los dotos.
    private VideoCassetteManager videoCassetteManager;

    // Method for get all the elements of the list. Because is HTTP we need to put this all here.
    // To download all the record i need to go to the endpoint with the name all.
    @Autowired
    public VideoCassetteApi(VideoCassetteManager videoCassetteManager)
    {
        this.videoCassetteManager = videoCassetteManager;
    }

    @GetMapping("/all")
    public
    Iterable<VideoCassette>getAll()
    {
        return videoCassetteManager.findAll();
    }

    // All of this to get the information from the API
    @GetMapping
    public Optional<VideoCassette> getById(@RequestParam Long index)
    {
        return videoCassetteManager.findById(index);
//        Optional<VideoCassette> first = videoCassettes.stream().
//                filter(element -> element.getId() == index).findFirst();
//        return first.get();
    }

    // Post information in the API, in the argument the element that i want to add. Request Body is because i want +.
    @PostMapping
    public VideoCassette addVideo(@RequestBody VideoCassette videoCassette)
    {
        return videoCassetteManager.save(videoCassette);
    }

    // To save the element, for example with the id.
    @PutMapping
    public VideoCassette updateVideo(@RequestBody VideoCassette videoCassette)
    {
        return videoCassetteManager.save(videoCassette);
    }

    // Delete element inside the API.
    @DeleteMapping
    public void deleteVideo(@RequestParam Long index)
    {
        videoCassetteManager.deteteById(index);
    }











}
