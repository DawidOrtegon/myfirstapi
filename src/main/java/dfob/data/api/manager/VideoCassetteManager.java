package dfob.data.api.manager;

import dfob.data.api.dataAccess.VideoCassetteRepo;
import dfob.data.api.dataAccess.entities.VideoCassette;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

// For the business logic.
@Service
public class VideoCassetteManager
{
    private VideoCassetteRepo videoCassetteRepo;

    @Autowired
    public VideoCassetteManager(VideoCassetteRepo videoCassetteRepo)
    {
        this.videoCassetteRepo = videoCassetteRepo;
    }

    // For an object variable because is the presentation of the information.
    public Optional<VideoCassette> findById(Long id)
    {
        return videoCassetteRepo.findById(id);
    }

    // To return some king of collection.
    public Iterable<VideoCassette> findAll()
    {
        return videoCassetteRepo.findAll();
    }

    // En la parte de command B se puede ver cual es el parametro que devuelve el parametro de la super clase.
    public VideoCassette save(VideoCassette videoCassette)
    {
        return videoCassetteRepo.save(videoCassette);
    }

    // Delete all the elements from the database.
    public void deteteById (Long id)
    {
        videoCassetteRepo.deleteById(id);
    }

    // Se ejecuta a penas la aplicacion se corre, Por lo que se vera dentro de la base de datos ya los records incluidos.
    @EventListener(ApplicationReadyEvent.class)
    public void fillDB()
    {
        save(new VideoCassette(1L,"Titanic", LocalDate.of(1995,12,12)));
        save(new VideoCassette(2L,"Fine Fiction", LocalDate.of(2000,1,10)));
    }

}
